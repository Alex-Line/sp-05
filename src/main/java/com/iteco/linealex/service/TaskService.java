package com.iteco.linealex.service;

import com.iteco.linealex.api.repository.ITaskRepository;
import com.iteco.linealex.api.service.ITaskService;
import com.iteco.linealex.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Collections;

@Transactional
@Service("taskService")
public final class TaskService extends AbstractTMService<Task> implements ITaskService {

    @Autowired
    private ITaskRepository taskRepository;

    @PostConstruct
    void init() {
        taskRepository.deleteAll();
    }

    @NotNull
    public Collection<Task> getAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return taskRepository.findAllByUserId(userId);
    }

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) return getAllEntities(userId);
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return taskRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public Task getEntityById(@Nullable final String entityId) {
        if (entityId == null) return null;
        return taskRepository.findById(entityId).get();
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntities() {
        return (Collection<Task>) taskRepository.findAll();
    }

    @Override
    @Transactional
    public void persist(@Nullable final Task entity) {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return;
        taskRepository.save(entity);
    }

    @Override
    public void persist(@NotNull Collection<Task> collection) {
        if (collection.isEmpty()) return;
        for (@Nullable final Task task : collection) {
            if (task == null) continue;
            persist(task);
        }
    }

    @Override
    @Transactional
    public void merge(@Nullable final Task entity) {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return;
        taskRepository.save(entity);
    }

    @Nullable
    @Override
    public Task getEntityByName(@Nullable final String entityName) {
        if (entityName == null || entityName.isEmpty()) return null;
        return taskRepository.findOneByName(entityName);
    }

    @Nullable
    @Override
    public Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    ) {
        if (userId == null || userId.isEmpty()) return null;
        if (entityName == null || entityName.isEmpty()) return null;
        return taskRepository.findOneByNameAndUserId(userId, entityName);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        return taskRepository.findAllByUserIdAndNameLike(userId, pattern);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(@Nullable final String pattern) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        return taskRepository.findAllByNameLike(pattern);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate() {
        return taskRepository.findAllSortedByDateStart();
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return taskRepository.findAllByUserIdSortedByDateStart(userId);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate() {
        return taskRepository.findAllSortedByDateFinish();
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return taskRepository.findAllByUserIdSortedByDateFinish(userId);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus() {
        return taskRepository.findAllSortedByStatus();
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return taskRepository.findAllByUserIdSortedByStatus(userId);
    }

    @Override
    @Transactional
    public void removeEntity(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return;
        taskRepository.deleteById(entityId);
    }

    @Override
    @Transactional
    public void removeAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void removeAllEntities() {
        taskRepository.deleteAll();
    }

    @Transactional
    public void removeAllTasksFromProject(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        taskRepository.deleteAllByUserIdAndProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskName
    ) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        return taskRepository.findOneByNameAndUserIdAndProjectId(userId, projectId, taskName);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String pattern
    ) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return taskRepository.findAllByUserIdAndProjectIdAndByNameLike(userId, projectId, pattern);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return taskRepository.findAllByUserIdAndProjectIdSortedByDateStart(userId, projectId);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        return taskRepository.findAllByUserIdAndProjectIdSortedByDateFinish(userId, projectId);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        return taskRepository.findAllByUserIdAndProjectIdSortedByStatus(userId, projectId);
    }

}