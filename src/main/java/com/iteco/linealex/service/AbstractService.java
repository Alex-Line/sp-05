package com.iteco.linealex.service;

import com.iteco.linealex.api.service.IService;
import com.iteco.linealex.model.AbstractEntity;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

@Getter
@Setter
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @Nullable
    @Override
    public abstract T getEntityById(@Nullable final String entityId);

    @NotNull
    @Override
    public abstract Collection<T> getAllEntities();

    @Override
    public abstract void persist(@Nullable final T entity);

    @Override
    public abstract void removeEntity(@Nullable final String entityId);

    public abstract void removeAllEntities();

}