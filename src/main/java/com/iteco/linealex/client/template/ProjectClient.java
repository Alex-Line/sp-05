package com.iteco.linealex.client.template;

import com.iteco.linealex.dto.ProjectDto;
import com.iteco.linealex.dto.UserDto;
import com.iteco.linealex.model.Project;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;

public class ProjectClient {

    public static void create(
            @NotNull final Project project,
            @NotNull final String secret
    ) {
        @NotNull final String url = "http://localhost:8080/api/project/create";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity<ProjectDto> entity = new HttpEntity<>(Project.toProjectDto(project), headers);
        template.postForObject(url, entity, Object.class);
    }

    public static Collection<ProjectDto> getAllProjects(@NotNull final String secret) {
        @NotNull final String url = "http://localhost:8080/api/project/list";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity requestEntity = new HttpEntity(null, headers);
        return (Collection<ProjectDto>) template.postForObject(url, requestEntity, Collection.class);
    }

    public static ProjectDto getProjectById(
            @NotNull final String id,
            @NotNull final String secret
    ) {
        @NotNull final String url = "http://localhost:8080/api/project/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity requestEntity = new HttpEntity(null, headers);
        return template.postForObject(url, requestEntity, ProjectDto.class, id);
    }

    public static void update(
            @NotNull final Project project,
            @NotNull final String secret
    ) {
        @NotNull final String url = "http://localhost:8080/api/project/update";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity<ProjectDto> entity = new HttpEntity<>(Project.toProjectDto(project), headers);
        template.put(url, entity);
    }

    public static void delete(
            @NotNull final String id,
            @NotNull final String secret
    ) {
        @NotNull final String url = "http://localhost:8080/api/project/delete/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity<UserDto> entity = new HttpEntity<>(null, headers);
        template.exchange(url, HttpMethod.DELETE, entity, String.class, id);
    }

}