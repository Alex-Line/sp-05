package com.iteco.linealex.client.template;

import com.iteco.linealex.dto.UserDto;
import com.iteco.linealex.enumerate.RoleType;
import com.iteco.linealex.model.Role;
import com.iteco.linealex.model.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;

public class UserClient {

    public static void create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String secret
    ) {
        @NotNull final String url = "http://localhost:8080/api/user/create";
        @NotNull final User user = new User(login, password);
        @NotNull final Role role = new Role(RoleType.ORDINARY_USER);
        role.setUser(user);
        user.getRoles().add(role);

        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity<UserDto> entity = new HttpEntity<>(User.toUserDto(user), headers);
        template.postForObject(url, entity, Object.class);
    }

    public static Collection<UserDto> getAllUsers(@NotNull final String secret) {
        @NotNull final String url = "http://localhost:8080/api/user/list";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity requestEntity = new HttpEntity(null, headers);
        return (Collection<UserDto>) template.postForObject(url, requestEntity, Collection.class);

    }

    public static UserDto getUserById(
            @NotNull final String id,
            @NotNull final String secret
    ) {
        @NotNull final String url = "http://localhost:8080/api/user/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity requestEntity = new HttpEntity(null, headers);
        return template.postForObject(url, requestEntity, UserDto.class, id);
    }

    public static UserDto getUserByLogin(
            @NotNull final String login,
            @NotNull final String secret
    ) {
        @NotNull final String url = "http://localhost:8080/api/user/find/{login}";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity requestEntity = new HttpEntity(null, headers);
        return template.postForObject(url, requestEntity, UserDto.class, login);
    }

    public static void update(
            @NotNull final User user,
            @NotNull final String secret
    ) {
        @NotNull final String url = "http://localhost:8080/api/user/update";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity<UserDto> entity = new HttpEntity<>(User.toUserDto(user), headers);
        template.put(url, entity);
    }

    public static void delete(
            @NotNull final String id,
            @NotNull final String secret
    ) {
        @NotNull final String url = "http://localhost:8080/api/user/delete/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity<UserDto> entity = new HttpEntity<>(null, headers);
        template.exchange(url, HttpMethod.DELETE, entity, String.class, id);
    }

}