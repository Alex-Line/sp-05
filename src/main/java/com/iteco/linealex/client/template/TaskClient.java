package com.iteco.linealex.client.template;

import com.iteco.linealex.dto.TaskDto;
import com.iteco.linealex.dto.UserDto;
import com.iteco.linealex.model.Task;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;

public class TaskClient {

    public static void create(
            @NotNull final Task task,
            @NotNull final String secret
    ) {
        @NotNull final String url = "http://localhost:8080/api/task/create";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity<TaskDto> entity = new HttpEntity<>(Task.toTaskDto(task), headers);
        template.postForObject(url, entity, Object.class);
    }

    public static Collection<TaskDto> getAllTasks(@NotNull final String secret) {
        @NotNull final String url = "http://localhost:8080/api/task/list";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity requestEntity = new HttpEntity(null, headers);
        return (Collection<TaskDto>) template.postForObject(url, requestEntity, Collection.class);
    }

    public static TaskDto getTaskById(
            @NotNull final String id,
            @NotNull final String secret
    ) {
        @NotNull final String url = "http://localhost:8080/api/task/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity requestEntity = new HttpEntity(null, headers);
        return template.postForObject(url, requestEntity, TaskDto.class, id);
    }

    public static void update(
            @NotNull final Task task,
            @NotNull final String secret
    ) {
        @NotNull final String url = "http://localhost:8080/api/task/update";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity<TaskDto> entity = new HttpEntity<>(Task.toTaskDto(task), headers);
        template.put(url, entity);
    }

    public static void delete(
            @NotNull final String id,
            @NotNull final String secret
    ) {
        @NotNull final String url = "http://localhost:8080/api/task/delete/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Cookie", secret);
        final HttpEntity<UserDto> entity = new HttpEntity<>(null, headers);
        template.exchange(url, HttpMethod.DELETE, entity, String.class, id);
    }

}