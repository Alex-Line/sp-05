package com.iteco.linealex.api.service;

import com.iteco.linealex.enumerate.RoleType;
import com.iteco.linealex.model.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IRoleService extends IService<Role> {

    @NotNull
    Collection<Role> getAllEntitiesById(@NotNull final Collection<String> collectionId);

    @NotNull
    Collection<Role> getAllEntitiesByUserId(@Nullable final String userId);

    @NotNull
    Role getRoleByType(RoleType roleType);

}