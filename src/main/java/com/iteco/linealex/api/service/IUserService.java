package com.iteco.linealex.api.service;

import com.iteco.linealex.model.User;
import org.jetbrains.annotations.Nullable;

public interface IUserService extends IService<User> {

    @Nullable
    public User logInUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception;

    void createUser(
            @Nullable final User user
    ) throws Exception;

    public void updateUserPassword(
            @Nullable final String oldPassword,
            @Nullable final String newPassword,
            @Nullable final User selectedUser
    ) throws Exception;

    @Nullable
    public User getUser(@Nullable final String login);

}