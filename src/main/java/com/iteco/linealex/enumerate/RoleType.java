package com.iteco.linealex.enumerate;

import org.jetbrains.annotations.NotNull;

public enum RoleType {

    ADMINISTRATOR("administrator"),
    ORDINARY_USER("ordinary user");

    @NotNull
    private String displayName;

    RoleType(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}