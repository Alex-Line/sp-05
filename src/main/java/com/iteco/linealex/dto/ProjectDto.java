package com.iteco.linealex.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.iteco.linealex.api.service.IUserService;
import com.iteco.linealex.enumerate.Status;
import com.iteco.linealex.model.Project;
import com.iteco.linealex.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Component
@NoArgsConstructor
public class ProjectDto {

    static IUserService staticUserService;

    @Autowired
    IUserService userService;

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    String name;

    @Nullable
    String description;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DD'T'hh:mm:ss±hh:mm")
    Date dateStart;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DD'T'hh:mm:ss±hh:mm")
    Date dateFinish;

    @Nullable
    String userId;

    Status status;

    @PostConstruct
    void init() {
        staticUserService = userService;
    }

    @Nullable
    public static Project toProject(@NotNull final ProjectDto projectDto) {
        @NotNull final Project project = new Project();
        project.setId(projectDto.getId());
        @Nullable final User user = staticUserService.getEntityById(projectDto.getUserId());
        if (user == null) return null;
        project.setUser(user);
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        project.setStatus(projectDto.getStatus());
        project.setDateStart(projectDto.getDateStart());
        project.setDateFinish(projectDto.getDateFinish());
        return project;
    }

}