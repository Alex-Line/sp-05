<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Task Manager</title>
</head>
<body>
    <div align="center">
        <h2>View Project</h2>
        <form:form method="get" modelAttribute="project">
            <table border="0" cellpadding="5">
                <tr>
                    <td>ID: </td>
                    <td>${project.id}</td>
                </tr>
                <tr>
                    <td>Name: </td>
                    <td>${projectName.name}</td>
                </tr>
                <tr>
                    <td>Description: </td>
                    <td>${project.description}</td>
                </tr>
                <tr>
                    <td>Start date: </td>
                    <td>${project.dateStart}</td>
                </tr>
                <tr>
                    <td>Finish date: </td>
                    <td>${project.dateFinish}</td>
                </tr>
                <tr>
                    <td>Status: </td>
                    <td>${project.status}</td>
                </tr>
                <tr>
                    <td>User ID: </td>
                    <td>${project.user.id}</td>
                </tr>
            </table>
        </form:form>
        <form method="get" action="/project/list">
            <button style="width:300x;height:50px" type="submit">Ok</button>
        </form>
        <a href="/logout"> Sign out</a>
    </div>
</body>
</html>