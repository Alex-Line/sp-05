<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Task Manager</title>
</head>
<body>
    <div align="center">
        <h2>Edit Task</h2>
        <form:form action="/project/save" method="post" modelAttribute="task">
            <table border="0" cellpadding="5">
                <tr>
                    <td>ID: </td>
                    <td>${task.id}
                        <form:hidden path="id"/>
                    </td>
                </tr>
                <tr>
                    <td>Name: </td>
                    <td><form:input path="name" /></td>
                </tr>
                <tr>
                    <td>Description: </td>
                    <td><form:input path="description" /></td>
                </tr>
                <tr>
                    <td>Start date: </td>
                    <td><form:input type="date" path="dateStart" /></td>
                </tr>
                <tr>
                    <td>Finish date: </td>
                    <td><form:input type="date" path="dateStart" /></td>
                </tr>
                <tr>
                    <td>Status: </td>
                    <td><form:input path="status" /></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Save"></td>
                </tr>
            </table>
        </form:form>
        <a href="/logout"> Sign out</a>
    </div>
</body>
</html>