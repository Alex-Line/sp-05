<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Task Manager</title>
</head>
<body>
<div align="center">
    <h1>Task List</h1>
    <h3><a href="/task/create">New Task</a></h3>
    <table border="1" cellpadding="5">
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Description</td>
            <td>Start date</td>
            <td>Finish date</td>
            <td>Status</td>
            <td>User ID</td>
            <td>Project ID</td>
            <td>Action</td>
        </tr>
            <c:forEach items="${tasks}" var="task">
                <tr>
                    <td>${task.id}</td>
                    <td>${task.name}</td>
                    <td>${task.description}</td>
                    <td>${task.dateStart}</td>
                    <td>${task.dateFinish}</td>
                    <td>${task.status}</td>
                    <td>
                        <a href="/task/view?userId=${project.user.id}&projectId=${project.id}">view</a>
                        <a href="/task/edit?userId=${project.user.id}&projectId=${project.id}">edit</a>
                        <a href="/task/delete?userId=${project.user.id}&projectId=${project.id}">delete</a>
                    </td>
                 </tr>
            </c:forEach>
    </table>
    <a href="/logout"> Sign out</a>
</div>
</body>
</html>