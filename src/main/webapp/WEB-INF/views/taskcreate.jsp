<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Task Manager</title>
</head>
<body>
    <div align="center">
        <h2>Create Task</h2>
        <form:form action="/task/save" method="post" modelAttribute="task">
            <table border="0" cellpadding="5">
                <tr>
                    <td>User login: </td>
                    <td><input type="text" name="userLogin" /></td>
                </tr>
                <tr>
                    <td>Project name: </td>
                    <td><input type="text" name="projectName" /></td>
                </tr>
                <tr>
                    <td>Name: </td>
                    <td><input type="text" name="name" /></td>
                </tr>
                <tr>
                    <td>Description: </td>
                    <td><input type="text" name="description" /></td>
                </tr>
                <tr>
                    <td>Start date: </td>
                    <td><input type="date" path="dateStart" name="dateStart" /></td>
                </tr>
                <tr>
                    <td>Finish date: </td>
                    <td><input type="date" path="dateFinish" name="dateFinish" /></td>
                </tr>
                <tr>
                    <td>Status: </td>
                    <td>
                        <form:select path="status" name="status">
                            <form:options items="${statuses}" />
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Save"></td>
                </tr>
            </table>
        </form:form>
        <a href="/logout"> Sign out</a>
    </div>
</body>
</html>