import com.iteco.linealex.client.template.ProjectClient;
import com.iteco.linealex.client.template.UserClient;
import com.iteco.linealex.dto.UserDto;
import com.iteco.linealex.enumerate.Status;
import com.iteco.linealex.model.Project;
import com.iteco.linealex.model.User;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ProjectIntegrationTest {

    @Nullable
    User user;

    @Nullable
    Project test;

    String secret;

    @Before
    public void setEnvironment() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/login?username=admin&password=11111111";
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
        secret = response.getHeaders().get("Set-Cookie").get(0).split(";")[0];
        UserClient.create("test", "11111111", secret);
        UserDto testDto = UserClient.getUserByLogin("test", secret);
        user = new User();
        user.setId(testDto.getId());
        user.setHashPassword(testDto.getHashPassword());
        user.setLogin(testDto.getLogin());
        test = new Project();
        test.setName("test");
        test.setDescription("test description");
        test.setUser(user);
        test.setStatus(Status.PLANNED);
        ProjectClient.create(test, secret);
    }

    @After
    public void cleanUp() {
        ProjectClient.delete(test.getId(), secret);
        UserClient.delete(user.getId(), secret);
    }

    @Test
    public void listProjectsIntegrationTestPositive() throws Exception {
        Assert.assertNotNull(ProjectClient.getAllProjects(secret));
        Assert.assertTrue(ProjectClient.getAllProjects(secret).toString().contains("test"));
    }

    @Test
    public void getProjectIntegrationTestPositive() throws Exception {
        Assert.assertNotNull(ProjectClient.getProjectById(test.getId(), secret).getId());
        Assert.assertTrue(ProjectClient.getProjectById(test.getId(), secret).getName().contains("test"));
    }

    @Test
    public void getProjectIntegrationTestNegative() throws Exception {
        Throwable thrown = assertThrows(Exception.class, () -> {
            ProjectClient.getProjectById(UUID.randomUUID().toString(), secret);
        });
        Assert.assertNotNull(thrown.getMessage());
    }

    @Test
    public void createProjectIntegrationTestPositive() throws Exception {
        Assert.assertNotNull(ProjectClient.getProjectById(test.getId(), secret).getId());
        Assert.assertTrue(ProjectClient.getProjectById(test.getId(), secret).getName().contains("test"));
    }

    @Test
    public void updateProjectIntegrationTestPositive() throws Exception {
        Assert.assertNotNull(ProjectClient.getProjectById(test.getId(), secret).getId());
        Assert.assertTrue(ProjectClient.getProjectById(test.getId(), secret).getName().contains("test"));
        test.setStatus(Status.DONE);
        ProjectClient.update(test, secret);
        Assert.assertNotNull(ProjectClient.getProjectById(test.getId(), secret).getId());
        Assert.assertSame(ProjectClient.getProjectById(test.getId(), secret).getStatus(), Status.DONE);
    }

    @Test
    public void updateProjectIntegrationTestNegative() throws Exception {
        Assert.assertNotNull(ProjectClient.getProjectById(test.getId(), secret).getId());
        Assert.assertTrue(ProjectClient.getProjectById(test.getId(), secret).getName().contains("test"));
        test.setStatus(Status.DONE);
        ProjectClient.update(test, secret);
        Assert.assertNotSame(ProjectClient.getProjectById(test.getId(), secret).getStatus(), Status.PROCESSING);
    }

}