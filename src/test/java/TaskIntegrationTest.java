import com.iteco.linealex.client.template.ProjectClient;
import com.iteco.linealex.client.template.TaskClient;
import com.iteco.linealex.client.template.UserClient;
import com.iteco.linealex.dto.UserDto;
import com.iteco.linealex.enumerate.Status;
import com.iteco.linealex.model.Project;
import com.iteco.linealex.model.Task;
import com.iteco.linealex.model.User;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class TaskIntegrationTest {

    @Nullable
    User user;

    @Nullable
    Project project;

    @Nullable
    Task test;

    String secret;

    @Before
    public void setEnvironment() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/login?username=admin&password=11111111";
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
        secret = response.getHeaders().get("Set-Cookie").get(0).split(";")[0];
        UserClient.create("test", "11111111", secret);
        UserDto testDto = UserClient.getUserByLogin("test", secret);
        user = new User();
        user.setId(testDto.getId());
        user.setHashPassword(testDto.getHashPassword());
        user.setLogin(testDto.getLogin());
        project = new Project();
        project.setName("test");
        project.setDescription("test description");
        project.setUser(user);
        project.setStatus(Status.PLANNED);
        ProjectClient.create(project, secret);
        test = new Task();
        test.setName("test");
        test.setDescription("test description");
        test.setUser(user);
        test.setProject(project);
        test.setStatus(Status.PLANNED);
        TaskClient.create(test, secret);
    }

    @After
    public void cleanUp() {
        TaskClient.delete(test.getId(), secret);
        ProjectClient.delete(project.getId(), secret);
        UserClient.delete(user.getId(), secret);
    }

    @Test
    public void listTasksIntegrationTestPositive() throws Exception {
        Assert.assertNotNull(TaskClient.getAllTasks(secret));
        Assert.assertTrue(TaskClient.getAllTasks(secret).toString().contains("test"));
    }

    @Test
    public void getTaskIntegrationTestPositive() throws Exception {
        Assert.assertNotNull(TaskClient.getTaskById(test.getId(), secret).getId());
        Assert.assertTrue(TaskClient.getTaskById(test.getId(), secret).getName().contains("test"));
    }

    @Test
    public void getTaskIntegrationTestNegative() throws Exception {
        Throwable thrown = assertThrows(Exception.class, () -> {
            TaskClient.getTaskById(UUID.randomUUID().toString(), secret);
        });
        Assert.assertNotNull(thrown.getMessage());
    }

    @Test
    public void createTaskIntegrationTestPositive() throws Exception {
        Assert.assertNotNull(TaskClient.getTaskById(test.getId(), secret).getId());
        Assert.assertTrue(TaskClient.getTaskById(test.getId(), secret).getName().contains("test"));
    }

    @Test
    public void updateTaskIntegrationTestPositive() throws Exception {
        Assert.assertNotNull(TaskClient.getTaskById(test.getId(), secret).getId());
        Assert.assertTrue(TaskClient.getTaskById(test.getId(), secret).getName().contains("test"));
        test.setStatus(Status.DONE);
        TaskClient.update(test, secret);
        Assert.assertNotNull(TaskClient.getTaskById(test.getId(), secret).getId());
        Assert.assertSame(TaskClient.getTaskById(test.getId(), secret).getStatus(), Status.DONE);
    }

    @Test
    public void updateTaskIntegrationTestNegative() throws Exception {
        Assert.assertNotNull(TaskClient.getTaskById(test.getId(), secret).getId());
        Assert.assertTrue(TaskClient.getTaskById(test.getId(), secret).getName().contains("test"));
        test.setStatus(Status.DONE);
        TaskClient.update(test, secret);
        Assert.assertNotSame(TaskClient.getTaskById(test.getId(), secret).getStatus(), Status.PROCESSING);
    }

}