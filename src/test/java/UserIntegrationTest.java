import com.iteco.linealex.client.template.UserClient;
import com.iteco.linealex.dto.UserDto;
import com.iteco.linealex.model.User;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class UserIntegrationTest {

    @Nullable
    User test;

    String secret;

    @Before
    public void setEnvironment() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/login?username=admin&password=11111111";
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
        secret = response.getHeaders().get("Set-Cookie").get(0).split(";")[0];
        UserClient.create("test", "11111111", secret);
        UserDto testDto = UserClient.getUserByLogin("test", secret);
        test = new User();
        test.setId(testDto.getId());
        test.setHashPassword(testDto.getHashPassword());
        test.setLogin(testDto.getLogin());
    }

    @After
    public void cleanUp() {
        UserClient.delete(test.getId(), secret);
    }

    @Test
    public void listUsersIntegrationTestPositive() {
        System.out.println(UserClient.getAllUsers(secret));
        Assert.assertNotNull(UserClient.getAllUsers(secret));
        Assert.assertTrue(UserClient.getAllUsers(secret).toString().contains("admin"));
    }

    @Test
    public void getUserIntegrationTestPositive() throws Exception {
        Assert.assertNotNull(UserClient.getUserById(test.getId(), secret));
        Assert.assertTrue(UserClient.getUserById(test.getId(), secret).getLogin().contains("test"));
    }

    @Test
    public void getUserIntegrationTestNegative() throws Exception {
        Throwable thrown = assertThrows(Exception.class, () -> {
            UserClient.getUserById(UUID.randomUUID().toString(), secret);
        });
        Assert.assertNotNull(thrown.getMessage());
    }

    @Test
    public void createUserIntegrationTestPositive() throws Exception {
        Assert.assertNotNull(UserClient.getUserById(test.getId(), secret));
        Assert.assertTrue(UserClient.getUserById(test.getId(), secret).getLogin().contains("test"));
    }

    @Test
    public void updateUserIntegrationTestPositive() throws Exception {
        Assert.assertNotNull(UserClient.getUserById(test.getId(), secret));
        Assert.assertTrue(UserClient.getUserById(test.getId(), secret).getLogin().contains("test"));
        test.setLogin("abra_kadabra");
        UserClient.update(test, secret);
        Assert.assertNotNull(UserClient.getUserById(test.getId(), secret));
        Assert.assertTrue(UserClient.getUserById(test.getId(), secret).getLogin().equals("abra_kadabra"));
    }

    @Test
    public void updateUserIntegrationTestNegative() throws Exception {
        Assert.assertNotNull(UserClient.getUserById(test.getId(), secret));
        Assert.assertTrue(UserClient.getUserById(test.getId(), secret).getLogin().contains("test"));
        test.setLogin("abra-kadabra");
        UserClient.update(test, secret);
        Assert.assertNotSame(UserClient.getUserById(test.getId(), secret).getLogin(), "test");
    }

}